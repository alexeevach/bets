package com.course.main;

import com.course.tools.Bet;
import com.course.tools.Client;
import com.course.tools.Game;
import com.course.tools.Member;

/**
 *
 * Есть игроки, которые могут делать ставки
 *         У игроков есть счет
 *         Не может делать ставку, если счет отрицательный
 * Есть участники игры с разными шансами на победу
 *         Суммарные шансы на победу не могут быть не равны 100%
 *         Участников не может быть меньше 2
 * Есть игра
 *         Для каждой игры устаналивается своя маржа от 1 до 20%
 *         Коэфициент на победу считается как 100 / (вероятность победы + маржа)
 *         Когда заканчивается игра - все поставившие на команду-победителя получают свои деньги
 *
 */

public class Main {
    public static void main(String... args) throws Exception {
        Client alisa = new Client("Alisa", 5000.0);
        Client bob = new Client("Bob", 2000.0);

        Member teamDragons = new Member("Team Dragons", 60);
        Member teamSharks = new Member("Team Sharks", 40);

        Game game = new Game(12.0, teamDragons, teamSharks);

        System.out.println("Alisa at the start: " + alisa);
        System.out.println("Bob at the start: " + bob);

        game.makeBet(Bet.makeBet(alisa, 2000.0, teamDragons));
        game.makeBet(Bet.makeBet(bob, 1000.0, teamSharks));

        System.out.println("Alisa after the bet: " + alisa);
        System.out.println("Bob after the bet: " + bob);

        game.setWinner("Team Sharks");

        System.out.println("Alisa at the end: " + alisa);
        System.out.println("Bob at the end: " + bob);
    }
}
