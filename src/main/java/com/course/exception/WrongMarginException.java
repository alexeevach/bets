package com.course.exception;

public class WrongMarginException extends Exception{
    public WrongMarginException(String message) {
        super(message);
    }
}
