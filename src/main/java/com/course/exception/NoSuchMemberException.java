package com.course.exception;

public class NoSuchMemberException extends Exception{
    public NoSuchMemberException(String message) {
        super(message);
    }
}
