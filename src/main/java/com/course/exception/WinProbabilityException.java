package com.course.exception;

public class WinProbabilityException extends Exception {
    public WinProbabilityException(String message) {
        super(message);
    }
}
