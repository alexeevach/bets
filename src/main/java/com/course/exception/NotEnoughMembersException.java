package com.course.exception;

public class NotEnoughMembersException extends Exception {
    public NotEnoughMembersException(String message) {
        super(message);
    }
}
