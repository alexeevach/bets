package com.course.exception;

public class MoneyException extends Exception {

    public MoneyException(String message) {
        super(message);
    }
}
