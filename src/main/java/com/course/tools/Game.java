package com.course.tools;

import com.course.exception.NoSuchMemberException;
import com.course.exception.NotEnoughMembersException;
import com.course.exception.WinProbabilityException;
import com.course.exception.WrongMarginException;

import java.util.*;

public class Game {

    private List<Member> members;
    private List<Bet> bets;
    private Double margin;

    public Game(Double margin, Member... members) throws WinProbabilityException, WrongMarginException, NotEnoughMembersException {
        if (margin < 1 || margin > 20) {
            throw new WrongMarginException("Margin should be between 1 and 20");
        }
        if (members.length < 2) {
            throw new NotEnoughMembersException("The amount of members should not be less than 2");
        }
        Integer totalPercent = 0;
        for(Member member : members) {
            totalPercent += member.getWinProbability();
        }
        if(totalPercent != 100) {
            throw new WinProbabilityException("Total win probability should be exactly 100%");
        }
        this.margin = margin;
        this.members = Arrays.asList(members);
        bets = new ArrayList<>();
    }

    public void makeBet(Bet bet) {
        bets.add(bet);
    }

    public void setWinner(String memberName) throws NoSuchMemberException {
        Optional<Member> winner = members.stream().filter(member -> member.getName().equals(memberName)).findFirst();
        if(winner.isPresent()) {
            Member actualWinner = winner.get();
            bets.stream()
                    .filter(bet -> bet.getMember().equals(actualWinner))
                    .forEach(bet -> {
                        Client client = bet.getPerson();
                        Double money = client.getMoney();
                        client.setMoney(money + calculateWin(bet.getBet(), actualWinner.getWinProbability()));
                    });
        } else {
            throw new NoSuchMemberException("No member with this name in the game: " + memberName);
        }
    }

    public Double calculateWin(Double money, Integer winProbability) {
        return 100.0 / (winProbability + margin) * money;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public Double getMargin() {
        return margin;
    }

    public void setMargin(Double margin) {
        this.margin = margin;
    }

    @Override
    public String toString() {
        return "Game{" +
                "members=" + members +
                ", bets=" + bets +
                ", margin=" + margin +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(members, game.members) &&
                Objects.equals(bets, game.bets) &&
                Objects.equals(margin, game.margin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(members, bets, margin);
    }
}
