package com.course.tools;

import com.course.exception.MoneyException;

import java.util.Objects;

public class Bet {

    private Client person;
    private Double bet;
    private Member member;

    private Bet(Client person, Double bet, Member member) {
        this.person = person;
        this.bet = bet;
        this.member = member;
    }

    public static Bet makeBet(Client person, Double bet, Member member) throws MoneyException {
        if (bet > person.getMoney()) {
            throw new MoneyException("Bet is bigger than amount of money for client " + person.getName());
        }
        Double money = person.getMoney();
        person.setMoney(money - bet);
        return new Bet(person, bet, member);
    }

    public Client getPerson() {
        return person;
    }

    public void setPerson(Client person) {
        this.person = person;
    }

    public Double getBet() {
        return bet;
    }

    public void setBet(Double bet) {
        this.bet = bet;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "person=" + person +
                ", bet=" + bet +
                ", member=" + member +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bet bet1 = (Bet) o;
        return Objects.equals(person, bet1.person) &&
                Objects.equals(bet, bet1.bet) &&
                Objects.equals(member, bet1.member);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, bet, member);
    }
}
