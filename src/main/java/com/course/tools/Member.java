package com.course.tools;

import java.util.Objects;

public class Member {

    private String name;
    private Integer winProbability;

    public Member(String name, Integer winProbability) {
        this.name = name;
        this.winProbability = winProbability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWinProbability() {
        return winProbability;
    }

    public void setWinProbability(Integer winProbability) {
        this.winProbability = winProbability;
    }

    @Override
    public String toString() {
        return "Member{" +
                "name='" + name + '\'' +
                ", winProbability=" + winProbability +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return Objects.equals(name, member.name) &&
                Objects.equals(winProbability, member.winProbability);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, winProbability);
    }
}
