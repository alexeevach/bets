package com.course.tools;

import com.course.exception.MoneyException;
import com.course.exception.NotEnoughMembersException;
import com.course.exception.WinProbabilityException;
import com.course.exception.WrongMarginException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BetTest {

    private static final Member TEST_MEMBER = new Member("TestMember", 20);

    private Client testClient;


    @BeforeEach
    public void setUp() {
        testClient = new Client("test1", 500.0);
    }

    @Test
    public void shouldSuccessfullyCreateBet_whenMakeBet_givenBetMoneyLessThanPersonalAccount() throws MoneyException {
        // given
        Double money = 200.0;
        // when
        Bet actualBet = Bet.makeBet(testClient, money, TEST_MEMBER);
        // then
        Assertions.assertEquals(actualBet.getBet(), money);
        Assertions.assertEquals(actualBet.getPerson(), testClient);
        Assertions.assertEquals(actualBet.getMember(), TEST_MEMBER);
    }

    @Test
    public void shouldThrowException_whenMakeBet_givenBetMoneyMoreThanPersonalAccount() {
        // given
        Double money = 600.0;
        // when
        MoneyException exception = Assertions.assertThrows(MoneyException.class,
                () -> Bet.makeBet(testClient, money, TEST_MEMBER));
        // then
        Assertions.assertEquals(exception.getMessage(), "Bet is bigger than amount of money for client test1");
    }

    @Test
    public void shouldTrowException_whenMakeBet_givenNegativeBet() {
        // given
        Double money = -100.0;
        // when
        MoneyException exception = Assertions.assertThrows(MoneyException.class,
                () -> Bet.makeBet(testClient, money, TEST_MEMBER));
        // then
        Assertions.assertEquals(exception.getMessage(), "Bet cannot be negative or zero");
    }

     @Test
     public void shouldAcceptTheSameValues_whenMakeBet_givenSameBet() throws MoneyException {
        // given
        Double money = 500.0;
        // when
        Bet acctualBet = Bet.makeBet (testClient, money, TEST_MEMBER);
        // then
        Assertions.assertEquals (acctualBet.getBet(), money);
        Assertions.assertEquals (acctualBet.getPerson(), testClient);
        Assertions.assertEquals (acctualBet.getMember(), TEST_MEMBER);
    }

    @Test
    public void whenMakeBet_givenNegativeBet() {
        //given
        Double money= 0.0;
        //when
        MoneyException exception = Assertions.assertThrows (MoneyException.class,
                () -> Bet.makeBet (testClient, money, TEST_MEMBER));
        // then
        Assertions.assertEquals (exception.getMessage(),"Bet cannot be negative or zero");
    }

    private Game testGame;

    @BeforeEach
    private void SetUp() throws WrongMarginException, NotEnoughMembersException, WinProbabilityException
    {testGame = new Game (20.00); testClient = new Client( "test", 200.00)}


}
